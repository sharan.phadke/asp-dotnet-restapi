namespace MyApi.Services;

public interface IWeatherService
{
    public IEnumerable<WeatherForecast> GetWeather();
}